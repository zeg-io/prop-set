const stringPathSet = (targetObjectRoot, stringPath, value, ignoreNull = true) => {
  const idxFirstDot = stringPath.indexOf('.'),
        property = stringPath.substr(0, idxFirstDot),
        restOfPath = stringPath.substr(idxFirstDot + 1)

  if (!value && ignoreNull) return

  if (property && restOfPath && property !== '') {
    if (!targetObjectRoot[property])
      targetObjectRoot[property] = {}
    stringPathSet(targetObjectRoot[property], restOfPath, value)
  } else if (!property && restOfPath && restOfPath !== '')
    targetObjectRoot[restOfPath] = value
  else
    targetObjectRoot[property] = value
}


module.exports = stringPathSet
